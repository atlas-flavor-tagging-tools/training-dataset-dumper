from dataclasses import dataclass
from typing import Mapping

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock


@dataclass
class GNNAuxTaskMapper(BaseBlock):
    """
    Schedule GNNAuxTaskDecoratorAlg. This alg reads aux task decos
    from the btagging object and copies them over to the tracks.
    For technical reasons (AFT-686) we can't decorate the track directly.

    Note that track_aux_tasks must be provided and the destination
    variable name must be different from the source variable name,
    which is used to decorate the btagging object, otherwise the 
    scheduler gets confused about the types.

    Parameters
    ----------
    btagging_container : str | None
        BTagging container name to read the aux-task outputs from.
    track_container : str
        Track container name to decorate with the aux-task outputs.
    track_links : str
        TrackLinks name associated with the BTagging object.
    track_aux_tasks : dict(str, str)
        Map between aux-task decorations for jets and decorations for tracks.
    """
    track_aux_tasks: Mapping[str, str]
    btagging_container: str = "BTagging_AntiKt4EMPFlow"
    track_container: str = "InDetTrackParticles"
    track_links: str = "GN2v01_TrackLinks"

    def __post_init__(self):
        self.deco_alg = CompFactory.FlavorTagDiscriminants.GNNAuxTaskDecoratorAlg
        self.name = 'GNNAuxTaskDecoratorAlg_' + '_'.join(self.track_aux_tasks.keys())

    def to_ca(self):
        ca = ComponentAccumulator()
        ca.addEventAlgo(
            self.deco_alg(
                name=self.name,
                btagging_container=self.btagging_container,
                track_container=self.track_container,
                track_links=self.track_links,
                track_aux_tasks=self.track_aux_tasks
            )
        )
        return ca
