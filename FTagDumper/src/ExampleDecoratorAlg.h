#ifndef EXAMPLE_ALG_H
#define EXAMPLE_ALG_H

#include "xAODBase/IParticleContainer.h"
#include "AthenaBaseComps/AthReentrantAlgorithm.h"

class ExampleDecoratorAlg: public AthReentrantAlgorithm
{
public:
  ExampleDecoratorAlg(const std::string& name, ISvcLocator* svcLocator);
  virtual StatusCode initialize () override;
  virtual StatusCode execute (const EventContext&) const override;
  virtual StatusCode finalize () override;
private:
  Gaudi::Property<float> m_decoratorValue {
    this, "decoratorValue", 43.4, "value to decorate"
  };
  SG::WriteDecorHandleKey<xAOD::IParticleContainer> m_exampleKey {
    this, "exampleDecorator", "", "an example decoration"
  };
};


#endif
