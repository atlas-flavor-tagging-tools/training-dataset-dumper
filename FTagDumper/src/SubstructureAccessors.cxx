#include "SubstructureAccessors.hh"
#include "xAODJet/Jet.h"

SubstructureAccessors::SubstructureAccessors():
  ecf1("ECF1"), ecf2("ECF2"), ecf3("ECF3"),
  tau1wta("Tau1_wta"), tau2wta("Tau2_wta"), tau3wta("Tau3_wta"),
  fw2("FoxWolfram2"), fw0("FoxWolfram0")
{}
float SubstructureAccessors::c2(const xAOD::Jet& j) {
  return ecf3(j) * ecf1(j) / pow(ecf2(j), 2.0);
}
float SubstructureAccessors::d2(const xAOD::Jet& j) {
  return ecf3(j) * pow(ecf1(j), 3.0) / pow(ecf2(j), 3.0);
}
float SubstructureAccessors::e3(const xAOD::Jet& j) {
  return ecf3(j)/pow(ecf1(j),3.0);
}
float SubstructureAccessors::tau21(const xAOD::Jet& j) {
  return tau2wta(j) / tau1wta(j);
}
float SubstructureAccessors::tau32(const xAOD::Jet& j) {
  return tau3wta(j) / tau2wta(j);
}
float SubstructureAccessors::fw20(const xAOD::Jet& j) {
  return fw2(j) / fw0(j);
}

